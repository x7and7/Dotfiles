(setq gc-cons-threshold (* 50 1000 1000))

;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  ;; (setq dashboard-banner-logo-title "\nKEYBINDINGS:\nOpen dired file manager  (SPC .)\nOpe")
  ;;(setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  (setq dashboard-startup-banner "~/.config/doom/doom-emacs-dash.png")  ;; use custom image as banner
  (setq dashboard-center-content nil) ;; set to 't' for centered content
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))

(setq doom-theme 'doom-one)
(map! :leader
      :desc "Load new theme" "h t" #'counsel-load-theme)

(setq doom-font (font-spec :family "Source Code Pro" :size 15)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 15)
      doom-big-font (font-spec :family "Source Code Pro" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;; Thanks, but no thanks
(setq inhibit-startup-message t)

  (scroll-bar-mode -1)        ; Disable visible scrollbar
  (tool-bar-mode -1)          ; Disable the toolbar
  (tooltip-mode -1)           ; Disable tooltips
  (set-fringe-mode 10)       ; Give some breathing room

(menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell t)

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 140
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

(column-number-mode)

;; Enable line numbers for some modes
(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

;; Override some modes which derive from the above
(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 1) ;; keyboard scroll one line at a time
  (setq use-dialog-box nil) ;; Disable dialog boxes since they weren't working in Mac OSX

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "<s-left>") 'windmove-left)
(global-set-key (kbd "<s-right>") 'windmove-right)
(global-set-key (kbd "<s-up>") 'windmove-up)
(global-set-key (kbd "<s-down>") 'windmove-down)

(use-package emojify
  :hook (after-init . global-emojify-mode))

(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))
(after! doom-themes
  (setq doom-neotree-enable-variable-pitch t))
(map! :leader
      :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
      :desc "Open directory in neotree" "d n" #'neotree-dir)

(setq org-directory "~/repos/Documents/")

(use-package org-mime
  :ensure t)

(defun my/org-mode/load-prettify-symbols () "Prettify org mode keywords"
  (interactive)
  (setq prettify-symbols-alist
    (mapcan (lambda (x) (list x (cons (upcase (car x)) (cdr x))))
          '(("#+begin_src" . ?)
            ("#+end_src" . ?)
            ("#+begin_example" . ?)
            ("#+end_example" . ?)
            ("#+DATE:" . ?⏱)
            ("#+AUTHOR:" . ?✏)
            ("[ ]" .  ?☐)
            ("[X]" . ?☑ )
            ("[-]" . ?❍ )
            ("lambda" . ?λ)
            ("#+header:" . ?)
            ("#+name:" . ?﮸)
            ("#+results:" . ?)
            ("#+call:" . ?)
            (":properties:" . ?)
            (":logbook:" . ?))))
  (prettify-symbols-mode 1))

(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)
(after! org
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  (setq org-directory "~/PersonalDocuments/"
        org-agenda-files '("~/PersonalDocuments/agenda.org")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-log-done 'time
        org-journal-dir "~/PersonalDocuments/journal/"
        org-journal-date-format "%B %d, %Y (%A) "
        org-journal-file-format "%Y-%m-%d.org"
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "FIX(f)"            ; Fixing anything in my system
             "WORK(w)"           ; Work to do
             "PROJ(p)"           ; A project that contains other tasks
             "VIDEO(v)"          ; Video to watch
             "REASONING(r)"      ; Reasoning
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has completed
             "FAILED(f)"         ; Task has failed
             "CANCELLED(c)" )))) ; Task has cancelled

(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.2))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.1))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.05))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
  '(org-level-6 ((t (:inherit outline-6 :height 1.0))))
  '(org-level-7 ((t (:inherit outline-7 :height 1.0))))
  '(org-level-8 ((t (:inherit outline-8 :height 1.0))))
)

(use-package ox-man)
(use-package ox-publish)

;; Ensure latest org-mode gets installed
(assq-delete-all 'org package--builtins)
(when (boundp 'org-package--builtin-versions)
  (assq-delete-all 'org package--builtin-versions))

(package-install 'org)

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/RoamNotes")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point)
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies) ;; Ensure the keymap is available
  (org-roam-db-autosync-mode))

(use-package! password-store)

(map! :leader
      (:prefix ("-" . "open file")
       :desc "Edit TODO.org" "t" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/TODO.org"))
       :desc "Edit Homework.org" "h" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/Homework.org"))
       :desc "Edit Physics homework" "P" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Physics.org"))
       :desc "Edit History homework" "H" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/History.org"))
       :desc "Edit Music homework" "M" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Music.org"))
       :desc "Edit Biology homework" "B" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Biology.org"))
       :desc "Edit RE homework" "R" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/RE.org"))
       :desc "Edit Art homework" "A" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Art.org"))
       :desc "Edit German homework" "G" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/German.org"))
       :desc "Edit Maths homework" "M" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Maths.org"))
       :desc "Edit PSHE homework" "F" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/PSHE.org"))
       :desc "Edit DT homework" "D" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/DT.org"))
       :desc "Edit Geography homework" "G" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/Geography.org"))
       :desc "Edit English homework" "E" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/School/English.org"))
       :desc "Edit git.org" "g" #'(lambda () (interactive) (find-file "~/repos/Documents/Home/Git.org"))
       :desc "Edit xmonad.hs" "x" #'(lambda () (interactive) (find-file "~/repos/.dotfiles/.xmonad/README.org"))
       :desc "Edit doom config.el" "c" #'(lambda () (interactive) (find-file "~/repos/.dotfiles/.config/doom/README.org"))
       :desc "Edit doom init.el" "i" #'(lambda () (interactive) (find-file "~/repos/.dotfiles/.config/doom/init.el"))
       :desc "Edit doom packages.el" "p" #'(lambda () (interactive) (find-file "~/repos/.dotfiles/.config/doom/packages.el"))))

(setq display-line-numbers-type t)
(map! :leader
      :desc "Comment or uncomment lines" "TAB TAB" #'comment-line
      (:prefix ("t" . "toggle")
       :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
       :desc "Toggle line highlight in frame" "h" #'hl-line-mode
       :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
       :desc "Toggle truncate lines" "t" #'toggle-truncate-lines))

(xterm-mouse-mode 1)

;; (defun efs/run-in-background (command)
;;   (let ((command-parts (split-string command "[ ]+")))
;;     (apply #'call-process `(,(car command-parts) nil 0 nil ,@(cdr command-parts)))))

;; (defun efs/set-wallpaper ()
;;   (interactive)
;;   ;; NOTE: You will need to update this to a valid background path!
;;   (start-process-shell-command
;;       "feh" nil  "feh --bg-scale /home/haiderm/Pictures/Wallpapers/0042.jpg"))

;; (defun efs/exwm-init-hook ()
;;   ;; Make workspace 1 be the one where we land at startup
;;   (exwm-workspace-switch-create 1)

  ;; Open eshell by default
  ;;(eshell)

  ;; ;; Show the time and date in modeline
  ;; (setq display-time-day-and-date t)
  ;; (display-time-mode 1)
  ;; ;; Also take a look at display-time-format and format-time-string

  ;; ;; Launch apps that will run in the background
  ;; (efs/run-in-background "nm-applet")
  ;; (efs/run-in-background "pasystray")
  ;; (efs/run-in-background "blueman-applet"))

;; (defun efs/exwm-update-class ()
;;   (exwm-workspace-rename-buffer exwm-class-name))

;; (defun efs/exwm-update-title ()
;;   (pcase exwm-class-name
;;     ("qutebrowser" (exwm-workspace-rename-buffer (format "qutebrowser: %s" exwm-title)))))

;; (defun efs/configure-window-by-class ()
;;   (interactive)
;;   (pcase exwm-class-name
;;     ("qutebrowser" (exwm-workspace-move-window 2))
;;     ("Firefox" (exwm-workspace-move-window 2))
;;     ("Thunar" (exwm-workspace-move-window 6))
;;     ("Microsoft Teams - Preview" (exwm-workspace-move-window 3))
;;     ("Lunar Client (1.8.9-44c40d4/master)" (exwm-workspace-move-window 5))))

;; (use-package exwm
;;   :config
  ;; ;; Set the default number of workspaces
  ;; (setq exwm-workspace-number 10)

  ;; ;; When window title updates, use it to set the buffer name
  ;; (add-hook 'exwm-update-title-hook #'efs/exwm-update-title)

  ;; ;; When window "class" updates, use it to set the buffer name
  ;; (add-hook 'exwm-update-class-hook #'efs/exwm-update-class)

  ;; ;; Configure windows as they're created
  ;; (add-hook 'exwm-manage-finish-hook #'efs/configure-window-by-class)

  ;; ;; When EXWM starts up, do some extra confifuration
  ;; (add-hook 'exwm-init-hook #'efs/exwm-init-hook)

  ;; ;; Set the wallpaper
  ;; (efs/set-wallpaper)

  ;; ;; Load the system tray before exwm-init
  ;; (require 'exwm-systemtray)
  ;; (setq exwm-systemtray-height 16)
  ;; (exwm-systemtray-enable)

  ;; ;; These keys should always pass through to Emacs
  ;; (setq exwm-input-prefix-keys
  ;;   '(?\C-x
  ;;     ?\C-u
  ;;     ?\C-h
  ;;     ?\M-x
  ;;     ?\M-`
  ;;     ?\M-&
  ;;     ?\M-:
  ;;     ?\C-\M-j  ;; Buffer list
  ;;     ?\C-\ ))  ;; Ctrl+Space

  ;; ;; Ctrl+Q will enable the next key to be sent directly
  ;; (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

  ;; (setq exwm-input-global-keys
  ;;       `(
  ;;         ;; Reset to line-mode (C-c C-k switches to char-mode via exwm-input-release-keyboard)
  ;;         ([?\s-r] . exwm-reset)

  ;;         ;; Move between windows
  ;;         ([s-left] . windmove-left)
  ;;         ([s-right] . windmove-right)
  ;;         ([s-up] . windmove-up)
  ;;         ([s-down] . windmove-down)

  ;;         ;; Launch applications via shell command
  ;;         ([?\s-&] . (lambda (command)
  ;;                      (interactive (list (read-shell-command "$ ")))
  ;;                      (start-process-shell-command command nil command)))

  ;;         ;; Switch workspace
  ;;         ([?\s-w] . exwm-workspace-switch)

  ;;         ;; 's-N': Switch to certain workspace with Super (Win) plus a number key (0 - 9)
  ;;         ,@(mapcar (lambda (i)
  ;;                     `(,(kbd (format "s-%d" i)) .
  ;;                       (lambda ()
  ;;                         (interactive)
  ;;                         (exwm-workspace-switch-create ,i))))
  ;;                   (number-sequence 0 9))))

  ;; (exwm-input-set-key (kbd "s-l") 'desktop-environment-lock-screen)
  ;; (exwm-input-set-key (kbd "s-b") 'counsel-switch-buffer)
  ;; (exwm-input-set-key (kbd "<s-delete>") 'delete-file)
  ;; (exwm-input-set-key (kbd "s-k") 'kill-buffer)
  ;; (exwm-input-set-key (kbd "s-s") 'swiper)
  ;; (exwm-input-set-key (kbd "s-SPC") 'counsel-linux-app)

  ;; (exwm-enable))

(setq shell-file-name "/bin/fish"
      vterm-max-scrollback 5000)
(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-aliases-file "~/.config/doom/eshell/aliases"
      eshell-history-size 5000
      eshell-buffer-maximum-lines 5000
      eshell-hist-ignoredups t
      eshell-scroll-to-bottom-on-input t
      eshell-destroy-buffer-when-process-dies t
      eshell-visual-commands'("bash" "fish" "htop" "ssh" "top" "zsh"))
(map! :leader
      :desc "Eshell" "e s" #'eshell
      :desc "Counsel eshell history" "e h" #'counsel-esh-history)

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
